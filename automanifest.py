import os
def main():
	while True:
		paket = input("Paketin Tam Adı: ")
		depends = input("R Depends: ").split(" ")
		popno = 0
		for depend in depends:
			if depend == "" or depend == " ":
				depends.pop(popno)
			popno += 1
		odepends = input("O Depends: ").split(" ")
		popno = 0
		for depend in odepends:
			if depend == "" or depend == " ":
				odepends.pop(popno)
			popno += 1
		trdesc = input("Turkish Description: ")
		endesc = input("English Description: ")
		sizep = float(input("Download Size: "))
		sizei = float(input("Installd Size: "))
		version = input("Package Version: ")
		os.system("mkdir {}".format(paket))
		os.system("rm -rf {}/manifest".format(paket))
		with open("{}/manifest".format(paket),"a") as f:
			f.write("depends{r}!")
			for depend in depends:
				f.write(depend)
				f.write(" ")
			f.write("\ndepends{o}!")
			for depend in odepends:
				f.write(depend)
				f.write(" ")
			f.write("\ndescrip{tr}!- "+trdesc+"\n")
			f.write("descrip{en}!- "+endesc+"\n")
			f.write("sizeofp{"+"{}".format("p")+"}"+"!{}\n".format(sizep))
			f.write("sizeofp{"+"{}".format("i")+"}"+"!{}\n".format(sizei))
			f.write("version{c}!"+version+"\n")
		os.system("cat {}/manifest".format(paket))
		print("nmanifest düzenleniyor...")
		with open("nmanifest","r") as f:
			paketler = f.readlines()[0].strip().split(" ")
		paketler.append(paket)
		paketler.sort()
		os.system("rm -rf nmanifest")
		with open("nmanifest","a") as f:
			for paketn in paketler:
				f.write(paketn)
				f.write(" ")
main()
