import os,hashlib
def main():
	with open("nmanifest","r") as f:
		packages = f.read().strip().split(" ")
		popno = 0
		for package in packages:
			if package == "" or "@" in package:
				packages.pop(popno)
			popno += 1
		del popno
	for package in packages:
		if "@" in package:
			continue
		packageSum = str(hashlib.sha1(open("{}/{}.evil.tar.zst".format(package,package),"br").read()).hexdigest())
		os.system("rm -rf {}/sha1sum".format(package))
		with open("{}/sha1sum".format(package),"a") as f:
			f.write(packageSum)
main()
